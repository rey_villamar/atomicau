<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'atomicau');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://localhost/atomicau');
define('WP_SITEURL','http://localhost/atomicau');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b;F.TlG^BOse7K]sW%gjE)uGLUn&4lHCNlVE`hLsh/kKZQ`LJ:9>|]8GRi<E(3fU');
define('SECURE_AUTH_KEY',  '`v!rOG84&6K$+Ck/A2@poz.LKQDp}.7}Q_b/T4}y-UYYi}wg/|N7)Y{o*BI9H]T`');
define('LOGGED_IN_KEY',    'g8HvR r.wCFkASnG:;@(Di+GswzE((O[eg:0Hj;#8uy^C/t[EBNqfea;Nhm n%/w');
define('NONCE_KEY',        '0e<Rx!wk7^Y-`BxUGWrS`#%iGh&l.im<iI?J;NEAgV9TP5i1q}Ut]AtBr(4rM#O(');
define('AUTH_SALT',        'w[x0 OJzW9ef@e5.Ki<NpSt,[f-WHytvcnGj_dc&#dS=0KmGt0HBW6a.bD^+^.D7');
define('SECURE_AUTH_SALT', 'EzrJkj;+df,i3%1uc0-_Z6k0I`(&Li,(@[58l7%?dpBC,2.}.c5rXq,P#A@7>cF&');
define('LOGGED_IN_SALT',   'k~.Dy`Py{rU}Z(Jf-J*)&&<+7P8&HDo.&|/ZY_}s?<k1K Vn+pnVExdV.bnNtqhR');
define('NONCE_SALT',       'i<XdRt?Zj.fn=Z]uL?}/N^rzzwqy>nXYy2*PH2e5+#E#@PhuMSbqS>)@6cNp<H@%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'au_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
